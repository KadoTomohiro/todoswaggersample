'use strict';


/**
 * Get All Todo List
 *
 * done Boolean todoリストをフィルタリングする (optional)
 * returns List
 **/
exports.todosGET = function(done) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "id" : "1",
  "done" : true,
  "task" : "朝会"
}, {
  "id" : "2",
  "done" : false,
  "task" : "夕会"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Post New Todo
 *
 * body Todo  (optional)
 * returns Todo
 **/
exports.todosPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "task" : "task",
  "id" : "id",
  "done" : false
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

