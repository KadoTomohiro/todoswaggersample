'use strict';

var utils = require('../utils/writer.js');
var Todo = require('../service/TodoService');

module.exports.todosGET = function todosGET (req, res, next) {
  var done = req.swagger.params['done'].value;
  Todo.todosGET(done)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.todosPOST = function todosPOST (req, res, next) {
  var body = req.swagger.params['body'].value;
  Todo.todosPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
